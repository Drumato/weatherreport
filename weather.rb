require 'json'
require 'net/http'
require 'terminal-table'

class WeatherReport
  SUMMARY = %w(date telop)

  def initialize(url)
    @weather = send_request(url)
    @title = @weather['title']
    puts weather_summary()
  end

  def self.action(method)
    self.send(method)
  end

  def send_request(path)
    response = Net::HTTP.get(URI.parse(path))
    weather = JSON.parse(response)
    return weather
  end

  def weather_summary
    tmp = Array.new
    infos = Array.new
    0.upto(1) do |idx|
      SUMMARY.each do |element|
        tmp << @weather['forecasts'][idx][element]
      end
    end
    infos.append(tmp)
    summary = Terminal::Table.new(title:@title,rows:infos)
    return summary
  end

  def make_table(info)
    hash = {'description'=>@weather[info]['text'],'link'=>@weather[info]}
    infos = Array.new
    infos.append([info,hash[info]])
    table = Terminal::Table.new(title:@title,rows:infos)
    return table
  end
end

def ui
  puts 'お天気データベースへようこそ!'
  puts '知りたい街の番号を入力してください!'
  puts Terminal::Table.new(title:'対応街一覧',rows:[['札幌','016010'],['東京','130010'],['横浜','140010'],['小田原','140020'],['大阪','270000'],['那覇','471010']])
  input = gets.chomp
  begin
    reporter = WeatherReport.new("http://weather.livedoor.com/forecast/webservice/json/v1?city=#{input}")
  rescue => err
    puts '異常終了します...'
    exit!
  end

  loop do
    puts '知りたい情報を入力してください!'
    puts '概況:description 詳細ページ:link プログラム終了:exit'
    input = gets.chomp
    exit! if input == 'exit'
    puts reporter.make_table(input)
  end
end

if __FILE__ == $0
  ui()
end
