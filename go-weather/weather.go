package main

import (
        "fmt"
        "github.com/olekukonko/tablewriter"
        "github.com/valyala/fastjson"
        "io/ioutil"
        "log"
        "net/http"
        "os"
)

const (
        URL = "http://weather.livedoor.com/forecast/webservice/json/v1?city="
)

func main() {
        if len(os.Args) < 2 {
                fmt.Println("Usage:./weather <citycode>")
                os.Exit(1)
        }
        client := &http.Client{}
        response := sendGetRequest(URL+os.Args[1], client)
        body, err := ioutil.ReadAll(response.Body)
        if err != nil {
                errorLog(err)
        }
        weather := parseJSON(string(body))
        if err != nil {
                errorLog(err)
        }
        title := string(weather.Get("title").GetStringBytes())
        description := string(weather.Get("description", "text").GetStringBytes())
        var content []string
        content = append(content, description)
        renderWeatherTable(title, content)
}

func sendGetRequest(url string, client *http.Client) *http.Response {
        req, err := http.NewRequest("GET", url, nil)
        if err != nil {
                errorLog(err)
        }
        resp, err := client.Do(req)
        if err != nil {
                errorLog(err)
        }
        return resp
}

func errorLog(err error) {
        log.Fatal(err)
        os.Exit(1)
}

func parseJSON(JSONString string) *fastjson.Value {
        var parser fastjson.Parser
        value, err := parser.Parse(JSONString)
        if err != nil {
                errorLog(err)
        }
        return value
}

func renderWeatherTable(title string, content []string) {
        writer := tablewriter.NewWriter(os.Stdout)
        writer.SetHeader([]string{title})
        writer.Append(content)
        writer.Render()
}