## **[WeatherReport](https://gitlab.com/Drumato/weatherreport)**

`JSON API`を触ったことが無かった事に危機感を持って､  
お手軽そうな[お天気API](http://weather.livedoor.com/weather_hacks/webservice)を使ってツールを作ってみた｡  

- **いくつかの都市のお天気**
- **詳しい概況**と**詳細ページのリンク**

を取ってくる簡素なものだが､JSONの便利さを知ることができた｡

**[詳細はこちら](https://drumato.hatenablog.com/entry/2018/11/27/130534)**から!

# Goで実装し直した

またGoのコードは**`.bashrc`**にエイリアス貼っとく事で､  
コマンドとして利用可能とした｡  

```bashrc
alias wtr='<バイナリパス>'
```

とすることで使える｡